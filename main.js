const express = require('express');
const app = express();

app.get('/', (req, res) => res.send('Hello World! from Node.js'));

app.listen(80, '0.0.0.0');

